package com.pds.CursoSpring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import primercodigo.beans.Barcelona;
import primercodigo.beans.Camiseta;
import primercodigo.beans.Jugador;
import primercodigo.beans.Juventus;
import primercodigo.beans.Marca;

@Configuration
public class AppConfig {
	
	@Bean
	public Jugador jugador1(){
		return new Jugador();
	}
	
	@Bean
	public Barcelona barcelona(){
		return new Barcelona();
	}
	
	@Bean
	public Barcelona barcelona1(){
		return new Barcelona();
	}
	
	@Bean
	public Juventus juventus(){
		return new Juventus();
	}
	
	@Bean
	public Camiseta camiseta(){
		return new Camiseta();
	}
	
	@Bean
	public Marca marca(){
		return new Marca();
	}
}
