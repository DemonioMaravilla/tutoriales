package com.pds.CursoSpring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pds.interfaces.IEquipo;

import primercodigo.beans.AppConfig;
import primercodigo.beans.AppConfig2;
import primercodigo.beans.Barcelona;
import primercodigo.beans.Ciudad;
import primercodigo.beans.Jugador;
import primercodigo.beans.Mundo;
import primercodigo.beans.Persona;

public class App {

	public static void main(String[] args) {
		

		ApplicationContext appContext = new ClassPathXmlApplicationContext("pds/xml/beans.xml");
		//Jugador jug =(Jugador) appContext.getBean("messi");
		Jugador jug =(Jugador) appContext.getBean("messi");
		//System.out.println(jug.getNombre() + "-" + jug.getEquipo().mostrar());
				
		System.out.println(jug.getNombre() + "-" + jug.getEquipo().mostrar());
		((ConfigurableApplicationContext)appContext).close();
	}

}
